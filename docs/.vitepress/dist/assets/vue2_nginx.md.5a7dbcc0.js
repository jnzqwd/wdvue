import{_ as s,o as n,c as a,V as l}from"./chunks/framework.f2d1e4b8.js";const A=JSON.parse('{"title":"nginx 配置","description":"","frontmatter":{},"headers":[],"relativePath":"vue2/nginx.md","filePath":"vue2/nginx.md","lastUpdated":1698047037000}'),e={name:"vue2/nginx.md"},t=l(`<h1 id="nginx-配置" tabindex="-1">nginx 配置 <a class="header-anchor" href="#nginx-配置" aria-label="Permalink to &quot;nginx 配置&quot;">​</a></h1><h2 id="实例" tabindex="-1">实例 <a class="header-anchor" href="#实例" aria-label="Permalink to &quot;实例&quot;">​</a></h2><div class="language-ini"><button title="Copy Code" class="copy"></button><span class="lang">ini</span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">server {</span></span>
<span class="line"><span style="color:#89DDFF;">  </span><span style="color:#676E95;font-style:italic;"># 开启服务器实时gzip</span></span>
<span class="line"><span style="color:#A6ACCD;">  gzip on</span><span style="color:#676E95;font-style:italic;">;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#89DDFF;">  </span><span style="color:#676E95;font-style:italic;"># 开启静态gz文件返回</span></span>
<span class="line"><span style="color:#A6ACCD;">  gzip_static on</span><span style="color:#676E95;font-style:italic;">;</span></span>
<span class="line"></span>
<span class="line"><span style="color:#A6ACCD;">  listen 80</span><span style="color:#676E95;font-style:italic;">;</span></span>
<span class="line"><span style="color:#A6ACCD;">  add_header Access-Control-Allow-Origin *</span><span style="color:#676E95;font-style:italic;">;</span></span>
<span class="line"><span style="color:#A6ACCD;">  location ~ ^/ziboGIS/dist.* {</span></span>
<span class="line"><span style="color:#89DDFF;">    </span><span style="color:#676E95;font-style:italic;"># root F:\\liantongCode\\code\\zibo\\social-emergency-web;</span></span>
<span class="line"><span style="color:#A6ACCD;">    proxy_pass http://127.0.0.1:5173</span><span style="color:#676E95;font-style:italic;">;</span></span>
<span class="line"><span style="color:#A6ACCD;">    proxy_http_version 1.1</span><span style="color:#676E95;font-style:italic;">; #这里必须使用http 1.1</span></span>
<span class="line"><span style="color:#89DDFF;">    </span><span style="color:#676E95;font-style:italic;">#下面两个必须设置，请求头设置为ws请求方式</span></span>
<span class="line"><span style="color:#A6ACCD;">    proxy_set_header Upgrade $http_upgrade</span><span style="color:#676E95;font-style:italic;">;</span></span>
<span class="line"><span style="color:#A6ACCD;">    proxy_set_header Connection </span><span style="color:#89DDFF;">&quot;</span><span style="color:#C3E88D;">upgrade</span><span style="color:#89DDFF;">&quot;</span><span style="color:#676E95;font-style:italic;">;</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span></code></pre></div>`,3),o=[t];function p(c,i,r,y,d,_){return n(),a("div",null,o)}const D=s(e,[["render",p]]);export{A as __pageData,D as default};
