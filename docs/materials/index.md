<style>
  .icon{
    width:20px;
    display:inline;
  }
</style>
# vue 物料市场
## <img src="https://element-plus.gitee.io/images/element-plus-logo-small.svg" class="icon"> element-plus
已经会了 Vue3 Element-plus？即刻阅读文档开始使用吧。[》》》》进入](https://element-plus.gitee.io)
## <img src="https://web-assets.dcloud.net.cn/unidoc/zh/icon.png?v=1556263038788" class="icon"> uniapp
uni-app代码编写，基本语言包括js、vue、css。以及ts、scss等css预编译器。[》》》》进入](https://uniapp.dcloud.net.cn/tutorial/)
## <img src="https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/da348fa1b980414384b25faa74d09a25~tplv-k3u1fbpfcp-zoom-in-crop-mark:1512:0:0:0.awebp?" class="icon"> autofit.js
autofit.js是一个可以让你的PC项目自适应屏幕的工具，其原理非常简单，即在scale等比缩放的基础上，向右或向下增加了宽度或高度，以达到充满全屏的效果，使用autofit.js不会挤压、拉伸元素，它只是单纯的设置了容器的宽高。[》》》》进入](https://www.npmjs.com/package/autofit.js)
## <img src="https://design.ksyun.com/imgs/favicon.ico" class="icon"> Kingcloud Design设计系统
全局提升团队协作，高效输出行业设计解决方案[》》》》进入](https://design.ksyun.com/docs/vue/)
## <img src="https://www.vexipui.com/vexip-ui.svg" class="icon"> vexipui
高度可定制化，全量 TypeScript，性能很不错[》》》》进入](https://www.vexipui.com/zh-CN/component/layout.html)
## <img src="https://manhattan.didistatic.com/static/manhattan/mand/docs/mand-logo-black.svg" class="icon"> mand-mobile
高度可定制化，全量 TypeScript，性能很不错[》》》》进入](https://didi.github.io/mand-mobile/#/zh-CN/docs/introduce)
## <img src="https://tauri.app/zh-cn/meta/favicon-32x32.png" class="icon"> tauri
构建跨平台的快速、安全、前端隔离应用[》》》》进入](https://tauri.app/zh-cn/v1/guides/getting-started/setup/vite)