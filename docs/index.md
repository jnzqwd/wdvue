---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "wdvue-doc"
  text: "组件推荐扩展库"
  tagline: 继承自element-plus
  image:
    src: https://vitejs.dev/logo-with-shadow.png
    alt: wdvue
  actions:
    - theme: brand
      text: 文档
      link: /vue/
    - theme: alt
      text: 物料市场
      link: /materials/

features:
  - icon: <img src="https://fesites.netlify.app/favicon.svg" width="20">
    title: 前端森林
    details: 拥有更多的前端知识
    link: https://fesites.netlify.app
  - icon: <img src="https://unocss.dev/logo.svg"  width="20"/>
    title: unocss
    details: UnoCSS是即时原子CSS引擎，旨在灵活且可扩展。核心是无主见的，所有的CSS实用程序都是通过预设提供的。
    link: https://unocss.dev/presets/wind
  - icon: <img src="https://element-plus-admin-doc.cn/logo.png" width="20">
    title: vue-element-plus-admin
    details: 一套基于vue3、element-plus、typesScript4、vite3的后台集成方案
    link: https://element-plus-admin-doc.cn
  - icon: <img src="https://v3.avuejs.com/images/logo.png" width="20">
    title: avue
    details: 前端搬砖神器,让数据驱动视图,更加贴合企业开发✨
    link: https://v3.avuejs.com/
  - icon: <img src="https://vueuse.org/favicon.svg" width="20">
    title: vue-use
    details: VueUse 是一个基于 Composition API 的实用程序函数集合。在继续之前，我们假设您已经熟悉组合 API 的基本思想。
    link: https://vueuse.org/guide/
  - icon: <img src="https://uiverse.io/favicon-32x32.png" width="20">
    title: uiverse
    details: 使用 HTML 和 CSS 制作的开源 UI 元素
    link: https://uiverse.io/all
  - icon: <img src="https://neumorphism.io/favicon.ico" width="20"/>
    title: Neumorphism.io
    details: 生成圆角边框 CSS 代码
    link: https://neumorphism.io
  - icon: <img src="/wdvue/img/mars3dLogo.png" width="20"/>
    title: Mar3D
    details: Mars3D三维可视化平台一款基于 WebGL 技术实现的三维客户端开发平台
    link: http://mars3d.cn/example.html
  - icon: <img src="https://p5.ssl.qhimg.com/t01c33383c0e168c3c4.png" width="20">
    title: SpriteJS
    details: 跨平台的高性能图形系统，它能够支持web、node、桌面应用和小程序的图形绘制和实现各种动画效果。
    link: http://spritejs.com/#/
  - icon: <img src="https://www.fusioncharts.com/public/favicon/apple-touch-icon.png" width="20">
    title: fusionCharts
    details: 从折线图、柱形图和饼图等简单图表到热图、雷达图和股票图等特定领域的图表，我们都能满足您的需求。立即开始使用FusionCharts构建您的仪表板。
    link: https://www.fusioncharts.com/charts/column-bar-charts/grouped-column-chart-in-3d-with-multiple-series?framework=javascript
  - icon: <img src="https://madeapie.com/favicon.ico" width="20">
    title: made a pie
    details: echart图表库
    link: https://madeapie.com/#/
  - icon: <img src="https://turfjs.fenxianglu.cn/assets/favicon.ico" width="20">
    title: Turf.js
    details: 地理空间分析库，处理各种地图算法
    link: https://turfjs.fenxianglu.cn/
  - icon: <img src="https://micro-frontends.ice.work/img/logo.png" width="20">
    title: icestark
    details: 在保证一个系统的操作体验基础上，实现各个微应用的独立开发和发版，主应用通过 icestark 管理微应用的注册和渲染，将整个系统彻底解耦。
    link: https://micro-frontends.ice.work/docs/guide
  - icon: <img src="https://zouyaoji.top/vue-cesium/favicon.png" width="20">
    title: CesiumJS
    details: 基于 Vue 3，面向开发者的 CesiumJS 组件库。
    link: https://zouyaoji.top/vue-cesium/#/zh-CN
  - icon: <img src="https://web-assets.dcloud.net.cn/unidoc/zh/icon.png?v=1556263038788" width="20">
    title: uniapp
    details: uni-app代码编写，基本语言包括js、vue、css。以及ts、scss等css预编译器。
    link: https://uniapp.dcloud.net.cn/tutorial/
  - icon: <img src="https://spline.design/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fspline_logo.647803e0.png&w=64&q=75" width="20">
    title: spline
    details: 3dweb建模工具，可以更好地与web端交互，并且缩短开发周期
    link: https://spline.design/#features
  - icon: <img src="https://threejs.org/files/favicon.ico" width="20">
    title: three.js
    details: 为了真正能够让你的场景借助three.js来进行显示，我们需要以下几个对象：场景、相机和渲染器，这样我们就能透过摄像机渲染出场景。
    link: https://threejs.org/docs/index.html#manual/zh/introduction/Installation
  - icon: <img src="https://element-plus.gitee.io/images/element-plus-logo-small.svg" width="20">
    title: element-plus
    details: 已经会了 Vue3 Element-plus？即刻阅读文档开始使用吧。
    link: https://element-plus.gitee.io/zh-CN/component/button.html
---
<style>
  :root{
    --vp-home-hero-name-color: transparent;
    --vp-home-hero-name-background:linear-gradient( 120deg, #bd34fe 30%, #41d1ff );
    --vp-home-hero-image-background-image: linear-gradient( -45deg, #bd34fe 50%, #47caff 50% );
    --vp-home-hero-image-filter: blur(72px);
    --vp-button-brand-bg: #41d1ff;
    --vp-button-brand-border: #41d1ff;
    --vp-button-brand-hover-border: #bd34fe;
    --vp-button-brand-hover-bg: #bd34fe;
  }
</style>
