# 关于移动端web在360手机浏览器上播放video视频时出现的bug
由于webstorm内置的tomcat不能在内网访问(vscode可以启动项目后访问),所以作者就直接将出现bug的页面放置到tomcat的webapps中,让处以同一网段的手机通过360手机浏览器访问(这里推荐在电脑上将对应的内网ip地址和项目地址合并后通过如草料二维码等网站生成二维码后手机直接扫码访问).
```html
.mp4 {
    height: 7rem;
    width: 75%;
    margin: auto;
    display: flex;
    justify-content: center;
    align-items: center;
    outline: none;
    overflow: hidden;
        }

<video id="myVideo"  poster="static/img/loginbackground.jpg"  style="outline: none;width: 100%;object-fit: fill;">
   <source src="static/mp4/arrow.mp4" type="video/mp4"/>
</video>
```
解决方案
``` ini
controls360=no  // 关闭360内置播放器,防止出现视频只显示一半另一半黑屏的bug
preload="none" // 页面加载后不加载视频,设置后可显示poster,猜测360手机浏览器在做适配时设置的默认参数不是none
```
加入后的代码:
``` html
<video id="myVideo" controls360=no preload="none" poster="static/img/loginbackground.jpg"  style="outline: none;width: 100%;object-fit: fill;">
   <source src="static/mp4/arrow.mp4" type="video/mp4"/>
</video>
```