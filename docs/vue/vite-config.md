# 配置vite.config.ts
## 安装插件
``` sh
pnpm add unplugin-auto-import unplugin-vue-components unocss vite-plugin-compression @vue/runtime-core sass
```
## 配置vite
``` ts
import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
//-------------需要自己配置
import AutoImport from 'unplugin-auto-import/vite'
import Unocss from 'unocss/vite'
import compressPlugin from 'vite-plugin-compression'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import ElementPlus from 'unplugin-element-plus/dist/vite.js'
//-------------需要自己配置
const env = loadEnv('development', './')

// https://vitejs.dev/config/
export default defineConfig({
  base: env.VITE_BASE_URL,
  plugins: [vue(), vueJsx(),
    Unocss(),
    AutoImport({//注册
      imports: ['vue','@vueuse/core','pinia',
      // 导入自定义的hooks
      {
      '@/hooks/request/request':['useRequest']
    }],
      dts: 'src/types/auto-import.d.ts',
      resolvers: [ElementPlusResolver()],
      eslintrc: {
        enabled: true, // Default `false`
        filepath: '/src/types/.eslintrc-auto-import.json', // Default `./.eslintrc-auto-import.json`
        globalsPropValue: true, // Default `true`, (true | false | 'readonly' | 'readable' | 'writable' | 'writeable')
      },
    }),
    Components({
      dts: 'src/types/components.d.ts',
      // 自动寻找的文件类型
      extensions: ['vue', 'tsx'],
      resolvers: [ElementPlusResolver({
        importStyle: "sass",
      }),
      /**
       * 自定义导出的组件名称
       */
      (componentName) => {
        // where `componentName` is always CapitalCase
        if (componentName.startsWith('Dv'))
          return { name: componentName.slice(2), from: '@kjgl77/datav-vue3' }
      }]
    }),
    compressPlugin({
      ext: '.gz',//gz br
      algorithm: 'gzip', //brotliCompress gzip
      // deleteOriginFile: true
    }),
    ElementPlus({
      useSource: true,
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@use "@/theme/index.scss" as *;`,
      },
    },
  },
  build:{
    outDir:'../webapp/admin-ui'
  }
})

```
## 创建一个scss
> 目录  src/theme/index.scss
``` scss
@forward 'element-plus/theme-chalk/src/common/var.scss' with (
  $colors: (
    'primary': (
      'base': #ff0000,
    ),
  ),
);
```