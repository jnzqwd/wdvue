# 安装必要插件
根据自己选择安装
## element-plus
### 完整引入
``` sh
pnpm i element-plus @element-plus/icons-vue -S
```
``` ts
// main.ts
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'

const app = createApp(App)

app.use(ElementPlus)
app.mount('#app')
```
tsconfig.json 配置
``` json
// tsconfig.json
{
  "compilerOptions": {
    // ...
    "types": ["element-plus/global"]
  }
}
```
### 按需导入

``` sh
npm install -D unplugin-vue-components unplugin-auto-import
```
#### vite环境配置
vite.config.ts 配置
``` ts
// vite.config.ts
import { defineConfig } from 'vite'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

export default defineConfig({
  // ...
  plugins: [
    // ...
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ],
})
```
#### nuxt环境配置
``` sh
npm install -D @element-plus/nuxt
```
nuxt.config.ts 配置
``` ts
// nuxt.config.ts
export default defineNuxtConfig({
  modules: ['@element-plus/nuxt'],
})
```
#### 图标加载
main.ts 配置
``` ts
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
```
``` ts
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
```
nuxt.config.ts 配置
``` ts
// nuxt.config.ts
export default defineNuxtConfig({
  modules: ['@element-plus/nuxt'],
})
```
## @vueuse/core
``` sh
pnpm i @vueuse/core @vueuse/integrations -S
```
## unocss
> 在配置vite.config.js，也添加了安装
``` sh
pnpm add -D unocss
```
vite.config.ts 配置
``` ts
// vite.config.ts
import UnoCSS from 'unocss/vite'
import { defineConfig } from 'vite'

export default defineConfig({
  plugins: [
    UnoCSS(),
  ],
})
```
main.ts 配置
``` ts
// main.ts
import 'virtual:uno.css'
```
在vscode中需要设置setting.json
``` json
{
  "editor.quickSuggestions": {
    "other": true,
    "comments": true,
    "strings": true
  }
}
```

## lodash
``` sh
pnpm add lodash
```
## qs
``` sh
pnpm add qs
```
## animate.css
``` sh
pnpm add animate.css
```
main.ts 配置
> 这是ts的方式唯一的导入方式，其他的会报css校验错误
``` ts
import 'animate.css/source/animate.css';
```
## vue3-layer
``` sh
pnpm add vue3-layer
```
引入方式
> 在项目中需要创建路径src\assets\theme\default，从node_modules找到theme\default，添加进去,否则打包会丢失
``` ts
import { layer } from "vue3-layer";
```
## axios
``` sh
pnpm add axios
```
引入方式
1. 在项目中需要创建路径src\hooks\request.ts
``` ts
import axios from "axios";
// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    return config;
  }, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  });

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    return response;
  }, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  });
export default axios
```
2. 在项目中需要创建路径src\api\api.ts
```ts
import axios from '@/hooks/request.ts'
export const getUser = axios.get('/user/12345')
export const postUser = axios.post('/user/12345')
```
## vxe-table

> 此组件比elementplus的表格功能更全，如果elementplus的表格满足不了，可以考虑，详情可以查看官网[vxe-table](https://vxetable.cn/#/table/start/install)，[xe-utils](https://vxetable.cn/xe-utils/#/)
``` sh
pnpm install xe-utils vxe-table -S
```
引入方式
> 在项目中需要创建路径main.ts
``` ts
import { App, createApp } = 'vue'
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'

function useTable (app: App) {
  app.use(VXETable)
}

createApp(App).use(useTable).mount('#app')
```
## wd3d

> 此组件是Mar3d的优化版，一直保持和Mars3d同步，可以消除掉logo
``` sh
pnpm install mars3d@pnpm:wd3d
```
引入方式
> 在项目中需要创建路径main.ts
``` ts
/**
 * 这样会引入所有资源
 */
import * as mars3d from 'mars3d'
/**
 * 这样只引入.d.ts
 */
import type * as mars3d from 'mars3d'
```