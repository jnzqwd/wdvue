# 快速上手

## 安装基础环境

>  node安装
1. 使用国内镜像[npmmirror](https://registry.npmmirror.com/binary.html?path=node),选择>16版本号。
2. 使用[nvm-window](https://github.com/coreybutler/nvm-windows/releases)安装，安装的时候，记得使用管理员身份进行切换。

``` sh
npm i pnpm -g

```
## 安装vue
``` sh
npm init vue@latest
```
``` sh
✔ Project name: … <your-project-name>
✔ Add TypeScript? … No / Yes
✔ Add JSX Support? … No / Yes
✔ Add Vue Router for Single Page Application development? … No / Yes
✔ Add Pinia for state management? … No / Yes
✔ Add Vitest for Unit testing? … No / Yes
✔ Add an End-to-End Testing Solution? … No / Cypress / Playwright
✔ Add ESLint for code quality? … No / Yes
✔ Add Prettier for code formatting? … No / Yes

Scaffolding project in ./<your-project-name>...
Done.
```

``` sh
cd <your-project-name>
pnpm install
pnpm dev
```