---
title: table表格
---
# table表格
## 基础
``` vue
<template>
  <avue-crud :option="option"
             :data="data"
             v-model:page="page"
             @search-change="searchChange"
             @search-reset="resetChange"
             @on-load="onLoad"
  >
  </avue-crud>
</template>
<script setup>
const page = ref({
  total: 1000,
  currentPage: 1,
  pageSize: 10
})
const data = ref([{
  name: '张三'
}])
const option = ref({
  column: [{
    label: '姓名',
    prop: 'name',
    search: true,
  }]
})
function resetChange (item) {
  ElMessage.success('清空回调')
}
function searchChange (params, done) {
  ElMessage.success('2s后关闭锁定')
  setTimeout(() => {
    done();
    ElMessage.success(JSON.stringify(params));
  }, 2000)

}
</script>
```