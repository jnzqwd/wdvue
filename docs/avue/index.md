---
title: 快速上手
---
# 快速上手
## 通过 npm 安装
在现有项目中使用 Avue 时，可以通过 npm 或 yarn 进行安装(需要先引入element-plus作为依赖支持)：

``` shell
npm i @smallwei/avue@next -S
pnpm i @smallwei/avue@next -S
yarn add @smallwei/avue@next -S
```
``` js
//需要先安装ElementPlus的依赖--如果使用auto-import不用导入
// import ElementPlus from 'element-plus'
// import 'element-plus/dist/index.css'
import {createApp} from 'vue'
import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
import axios from 'axios'
const app =createApp({})
// app.use(ElementPlus)
app.use(Avue, {axios});
```