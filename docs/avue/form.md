---
title: form表单
---
# form表单
## 基础表单
``` vue
 <template>
 <avue-form v-model="form" :option="option" @submit="submit" @error="error" />
 </template>
 <script setup>
 const form = ref({})
  const option = ref({
    column: [
          {
            label: '姓名',
            prop: 'name',
            rules: [{
              required: true,
              message: "请输入姓名",
              trigger: "blur"
            }]
          }, {
            label: '性别',
            prop: 'sex',
            rules: [{
              required: true,
              message: "请输入性别",
              trigger: "blur"
            }]
          }
        ]
  })
  function submit(form, done){}
  function error(err){}
 </script>
 ```

## 表单自定义
``` vue
 <template>
 <avue-form v-model="form" :option="option" @submit="submit" @error="error">
  <template #name="{disabled,size}">
      <div>
        <el-tag>{{form.text?form.text:'暂时没有内容'}}</el-tag>
        <el-input :disabled="disabled"
                  :size="size"
                  v-model="form.text"
                  placeholder="这里是自定的表单"></el-input>
      </div>
    </template>
 </avue-form>
 </template>
 <script setup>
 const form = ref({})
  const option = ref({
    column: [
          {
            label: '姓名',
            prop: 'name',
          },
          {
            label: '',
            labelWidth: 40,
            prop: 'calendar',
            // 组件名称：如果有v-model会自动绑定
            component: 'elCalendar',//ele日期
            span: 24,
            params: {
              // 除了v-model以外的属性
            }
          }
        ]
  })
  function submit(form, done){}
  function error(err){}
 </script>
 ```

 ## 表单组件事件
``` vue
 <template>
 <avue-form v-model="form" :option="option" @submit="submit" @error="error" />
 </template>
 <script setup>
 const form = ref({})
  const option = ref({
    column: [
          {
            label: '姓名',
            prop: 'name',
              change: ({ value, column }) => {
              this.$message.success('change事件查看控制台')
              console.log('值改变', value, column)
            },
            click: ({ value, column }) => {
              this.$message.success('click事件查看控制台')
              console.log('点击事件', value, column)
            },
            focus: ({ value, column }) => {
              this.$message.success('focus事件查看控制台')
              console.log('获取焦点', value, column)
            },
            blur: ({ value, column }) => {
              this.$message.success('blur事件查看控制台')
              console.log('失去焦点', value, column)
            },
            enter: ({ value, column }) => {
              this.$message.success('enter事件查看控制台')
              console.log('回车事件', value, column)
            }
          }
        ]
  })
  function submit(form, done){}
  function error(err){}
 </script>
 ```
 