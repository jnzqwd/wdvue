# vscode插件

## TypeScript Vue Plugin (Volar)
vue必要的
## unocss
unocss必要的
## Vue Language Features (Volar)
vue必要的
## 英文驼峰命名
自动把中文翻译成英文
## auto close Tag
自动关闭标签
## auto Rename Tag
自动改变命名
## chatGpt Gpt-4
ai智能
## Chinese (Simplified) (简体中文) Language Pack f
中文
## editorConfig for vs code
编辑器缩进配置
## ESlint
eslint配置
## IDEA-Icons-To-Maven-For-VSCode
idea主题
## Intellij+VsCode Soft Dark Theme
idea主题
## JS To TS
转ts声明文件
## letter-transform
驼峰转中划线
## Tabnine AI Autocomplete for Javascript, Python, 
ai智能
## wink-snippets
最新的vue3代码提示器
## Local History (New)
本地历史记录


vite-plugin-rsw
适用于 Vite 的 wasm-pack 插件

npm npm downloads vite version chat

awesome-rsw Rust WebAssembly

RSW 版本	维特版本
>= 2.0.0	>= 2.8.0
>= 1.8.0	>= 2.4.0
1.7.0	2.0.0 ~ 2.3.8
特征
HMR
Friendly error message- 浏览器和终端
预装
锈
节点JS目录
瓦斯姆包
rsw-rs： - 一个命令行工具，用于基于 wasm-pack 实现自动重建本地更改。rsw = rs(rust) → w(wasm)
用法
步骤 1
安装 rsw

cargo install rsw
安装 vite-plugin-rsw

# With NPM:
npm i -D vite-plugin-rsw

# With Yarn:
yarn add -D vite-plugin-rsw
步骤 2
编辑 vite.config.ts

import { defineConfig } from 'vite';
import { ViteRsw } from 'vite-plugin-rsw';

export default defineConfig({
  plugins: [
    ViteRsw(),
  ],
})
步骤 3
编辑包.json

建议使用而不是并发运行命令，因为它无法正确处理系统信号，请参阅 rwasm/rsw-rs#7concurrently&

"scripts": {
+   "dev": "concurrently \"rsw watch\" \"vite\"",
+   "build": "rsw build && tsc && vite build",
+   "rsw": "rsw"
}
步骤 4
rsw.toml 选项

初始 rsw.toml

# yarn rsw -h
yarn rsw init
生成防锈箱

# rsw.toml
[new]
# using: wasm-pack | rsw | user, default is `wasm-pack`
using = "wasm-pack"
yarn rsw new rsw-hello
编辑 rsw.toml

# link type: npm | yarn | pnpm, default is `npm`
cli = "npm"

[[crates]]
name = "rsw-hello"
# <npm|yarn|pnpm> link
# ⚠️ Note: must be set to `true`, default is `false`
link = true
步骤 5
启动开发服务器

# rsw watch & vite
yarn dev
步骤 6
部署

yarn build
例
贾夫 - 🤩使网页更像桌面应用程序只是一个开始，可能性是无限的，取决于你的想象力！
演示 - 🎲学习网络组装
哦，我的盒子 - 🔮开发工具箱，以及更多...
相关
创建-MPL - ⚡️在几秒钟内创建一个项目！

# Quickly initialize a wasm project

# npm 6.x
npm init mpl@latest my-app --type wasm

# npm 7+, extra double-dash is needed:
npm init mpl@latest my-app -- --type wasm
微信
群二维码已过期，关注公众号《浮之静》，发送"进群"，我将拉你进群一起学习。

wasm-wechat-qrcode fzj-qrcode

许可证
麻省理工学院许可证 © 2021 lencx