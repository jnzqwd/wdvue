# nginx 配置

## 实例
``` ini
server {
  # 开启服务器实时gzip
  gzip on;

  # 开启静态gz文件返回
  gzip_static on;

  listen 80;
  add_header Access-Control-Allow-Origin *;
  location ~ ^/ziboGIS/dist.* {
    # root F:\liantongCode\code\zibo\social-emergency-web;
    proxy_pass http://127.0.0.1:5173;
    proxy_http_version 1.1; #这里必须使用http 1.1
    #下面两个必须设置，请求头设置为ws请求方式
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
  }
}
```