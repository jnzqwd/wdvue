# 配置tsconfig.json

## 配置tsconfig.json
``` json
{
  "compilerOptions": {"forceConsistentCasingInFileNames": true,"strict": true},
  "typeAcquisition": {"enable": true},
  "files": [],
  "references": [
    {
      "path": "./tsconfig.node.json"
    },
    {
      "path": "./tsconfig.app.json"
    }
  ]
}

```
## 配置tsconfig.app.json
``` json
{
  "extends": "@vue/tsconfig/tsconfig.dom.json",
  "include": ["env.d.ts", "src/**/*"],
  "exclude": ["src/**/__tests__/*","animate.css","font"],
  "compilerOptions": {
    "module": "ESNext",
    "moduleResolution": "node",
    "composite": true,
    "baseUrl": ".",
    "paths": {
      "@/*": ["./src/*"]
    }
  }
}

```
## 配置tsconfig.node.json
``` json
{
  "extends": "@tsconfig/node18/tsconfig.json",
  "include": ["vite.config.*", "vitest.config.*", "cypress.config.*", "playwright.config.*"],
  "compilerOptions": {
    "lib": ["ESNext"],
    "module": "ESNext",
    "moduleResolution": "Node", 
    "composite": true,
    "types": ["node"]
  }
}


```