# 快速上手

## 安装基础环境

>  node安装
1. 使用国内镜像[npmmirror](https://registry.npmmirror.com/binary.html?path=node),选择>16版本号。
2. 使用[nvm-window](https://github.com/coreybutler/nvm-windows/releases)安装，安装的时候，记得使用管理员身份进行切换。

## 基础知识
vue(https://v2.cn.vuejs.org/v2/guide/installation.html)

## vue2升级2.7

### 安装 vue，vue-demi，vue-frag，vue2-teleport-component，@vueuse/core
```sh
npm i vue vue-demi vue-frag vue2-teleport-component @vueuse/core -S
```
```sh
npm i vue vue-template-compiler unplugin-vue-define-options -D
```
### vue配置--vue.config.js

```js
configureWebpack: {
  plugins: [
      require('unplugin-vue-define-options/webpack')()
    ]
}
```