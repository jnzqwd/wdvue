# 快速上手
## 先通过vue模块安装基础环境
请查看本网站[vue](/vue/index.html)安装
## 安装rust
[rust](https://www.rust-lang.org/zh-CN/tools/install)
## 安装rust插件（vscode）
[rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rustcc.rust-analyzer-cn)
## 安装wasm-pack
[wasm-pack](https://rustwasm.github.io/docs/wasm-pack/quickstart.html)
## 安装rsw
使用命令行安装
```shell
cargo install rsw
```
## vite-plugin-rsw
使用命令行安装
```shell
pnpm install vite-plugin-rsw
```

## 编辑 vite.config.js

```ts
import { defineConfig } from 'vite'
import { ViteRsw } from 'vite-plugin-rsw'; // ✅ 新增

export default defineConfig({
  plugins: [
    ViteRsw(), // ✅ 新增
  ],
})
```

## 编辑 package.json
```ts
{
  "scripts": {
    "dev": "vite",
    "build": "rsw build && tsc && vite build",
    "preview": "vite preview",
    "tauri": "tauri",
    "rsw": "rsw"
  }
}
```

## 初始化 rsw 配置

生成 rsw.toml

``` sh
pnpm rsw init
```
## 新建 wasm 项目
内置三种模式，可以通过编辑 rsw.toml > [new] > using 来切换

``` sh
pnpm rsw new @mywasm/foo
```
也可以在 rsw.toml > [[crates]] 中新增 @mywasm/foo
```yml
#! link 类型: `npm` | `yarn` | `pnpm`, 默认是 `npm`
cli = "npm"

[new]
#! 使用: `wasm-pack` | `rsw` | `user`, 默认 `wasm-pack`
#! 1. wasm-pack: `rsw new <name> --template <template> --mode <normal|noinstall|force>`
#! 2. rsw: `rsw new <name>`, 内置模板
#! 3. user: `rsw new <name>`，如果 `dir`未配置, 则使用 `wasm-pack new <name>`初始化 wasm 项目
using = "rsw"
#! 当 `using = "user"` 时，`dir` 才会生效
#! 如果 `using = "wasm-pack"` 或 `using = "rsw"`，则忽略
#! 复制此目录下的所有文件到初始化的 wasm 项目中
dir = "my-template"

[[crates]]
#! npm 包，@mywasm 为 npm 组织，foo 是该组织下的包名
name = "@mywasm/foo"
#! 是否执行 link， link 类型通过 `cli` 配置
link = true

[[crates]]
#! npm 包，@mywasm 为 npm 组织，bar 是该组织下的包名
name = "@mywasm/bar"
#! 是否执行 link， link 类型通过 `cli` 配置
link = true
```

## 开发模式
执行以下命令，不要退出：
```bash
pnpm rsw watch
```
 新开一个命令行窗口，执行 a 或 b：
```sh
pnpm dev
```
> 以下步骤在rust项目目录下执行

## 添加web_sys插件，为了操作dom
在cargo.toml添加
```toml
[dependencies.js-sys]
[dependencies.web-sys]
version = "0.3.22"
features = [
  'CanvasRenderingContext2d',
  'CssStyleDeclaration',
  'Document',
  'Element',
  'EventTarget',
  'HtmlCanvasElement',
  'HtmlElement',
  'MouseEvent',
  'Node',
  'Window',
  'console'
]
```
## 加载cargo的安装包
```bash
cargo c
```
## 加载rtml模版框架--web操作使用
```bash
cargo add rtml
```
## 构建
```sh
pnpm run build
```
## 推荐目录结构
```sh
[wasm-app] # 项目名称
│ # 新增结构
├─ [@mywasm] # ✅ 组织名称（npm org）
│    ├─ [foo] # ✅ @mywasm/foo - wasm 包名
│    └─ [bar] # ✅ @mywasm/bar - wasm 包名
├─ [.rsw] # ✅ rsw 临时文件夹
├─ rsw.toml # ✅ rsw 配置文件
├─ .watchignore # ✅ rsw watch 忽略文件
│┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈
│ # 原结构
├─ [node_modules] # 前端依赖
├─ [src] # 前端程序源
├─ index.html # 项目主界面
├─ package.json # 前端项目清单
├─ tsconfig.json # typescript 配置文件
├─ vite.config.ts # vite 配置文件
└─ ... # 其他
```