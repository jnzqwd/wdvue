import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "wdvue-doc",
  description: "wdvue-doc",
  base: '/',
  lastUpdated: true,
  head: [[
    'link',
    { rel: "icon", href: "https://vitejs.dev/logo-with-shadow.png" }
  ]],
  themeConfig: {
    lastUpdatedText:"上次更新",
    logo: 'https://vitejs.dev/logo-with-shadow.png',
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: '首页', link: '/' },
      { text: 'vue3安装', link: '/vue/', activeMatch: '/vue/' },
      { text: 'wasm安装', link: '/wasm/', activeMatch: '/wasm/' },
      { text: '物料市场', link: '/materials/', activeMatch: '/materials/' },
      { text: 'java问题', link: '/java/', activeMatch: '/java/' },
      { text: '移动端问题', link: '/app/', activeMatch: '/app/' },
      { text: 'vscode插件', link: '/vscode/', activeMatch: '/vscode/' },
    ],

    sidebar: {
      '/vue': [
        {
          text: 'vue3安装',
          items: [
            { text: '安装', link: '/vue/' },
            { text: '规范', link: '/vue/guide' },
            { text: '配置tsconfig.json', link: '/vue/tsconfig' },
            { text: '安装必要插件', link: '/vue/plug-in' },
            { text: '安装附加插件', link: '/vue/additional' },
            { text: '配置vite.config.ts', link: '/vue/vite-config' },
            { text: '配置nginx', link: '/vue/nginx' },
          ]
        }
      ],
      '/uniapp': [
        {
          text: 'avue扩展库',
          items: [
            { text: '快速上手', link: '/uniapp/index.html' },
            { text: 'form表单', link: '/uniapp/form' },
            { text: 'table表格', link: '/uniapp/table' },
          ]
        }
      ],
      '/wasm': [
        {
          text: 'wasm',
          items: [
            { text: '快速上手', link: '/wasm/index.html' },
            // { text: 'rust基础语法', link: '/wasm/grammar' },
            // { text: '操作dom', link: '/wasm/dom' },
          ]
        }
      ],
      '/java': [
        {
          text: 'java面试',
          items: [
            { text: 'redis', link: '/java/index.html' },
            { text: 'form表单', link: '/java/form' },
            { text: 'table表格', link: '/java/table' },
          ]
        }
      ],
      '/app': [
        {
          text: 'app问题',
          items: [
            { text: '1.移动端在360手机浏览器上播放video视频时出现的bug', link: '/app/index.html' },
            { text: 'pc端', link: '/web/pc' },
          ]
        }
      ],
      '/materials': [{
        text: 'vue物料市场',
        link: '/materials/index.html'
      }, {
        text: '地图物料市场',
        link: '/materials/map'
      }, {
        text: '图表物料市场',
        link: '/materials/chart'
      }, {
        text: 'uniapp物料市场',
        link: '/materials/uniapp'
      }],
      '/vscode': [
        {
          text: 'vscode插件',
          items: [
            { text: 'vscode', link: '/vscode/index.html' },
          ]
        }
      ]
    },

    socialLinks: [
      // { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ],
    footer: {
      message: '此文档自己整理，一部分摘自网络开源部分，一部分属于个人所有',
      copyright: 'Copyright © 2023-present wangdong'
    },
    search: {
      provider: 'local'
    },
  }
})
